#!/usr/bin/env python3
from urllib import request, parse
from bs4 import BeautifulSoup    
import base64
from pyzbar.pyzbar import decode
from PIL import Image
import numpy as np
import requests
import os
cookie = {'PHPSESSID' :'ps348ktoengkphmh6gtkkp7ct0', 'uid':'wKgbZFxcFJoEsANvG4tqAg=='}
headers = {
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:63.0) Gecko/20100101 Firefox/63.0',
        'Accept-Language': 'en-US,en;q=0.5',
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
        'Accept-Encoding': 'gzip, deflate',
        'Content-Type': 'application/x-www-form-urlencoded',
        'Connection':'keep-alive'
    }



def main():
    s = requests.Session()
    response = s.get('http://challenge01.root-me.org/programmation/ch7/',cookies=cookie, headers=headers)
    binary_image = binary_image_find(response)
    write_img(binary_image)
    combine_images()
    Decoded_data = decode(Image.open('./full_qrcode.png'))
    result = str(Decoded_data[0][0]).split(' ')[-1][:-1]
    print(result)
    os.remove('./full_qrcode.png')
    data = parse.urlencode({"metu":result})
    req =  s.post("http://challenge01.root-me.org/programmation/ch8/", data=data, cookies=cookie, headers=headers)
    # if ('Failed' in req.text):
    #     main(req)
    #     continue
    # else:
    print(req.text)

def write_img(binary_image):
    x = np.fromstring(binary_image, dtype=np.uint8)
    out_bytes = np.unpackbits(x)
    out_bytes = np.packbits(out_bytes)
    out_bytes.tofile('qrcode.png')

def combine_images():
     main_img = Image.open('./qrcode.png')
     qr_dots =  Image.open('./qr_dots.png')
     main_img.paste(qr_dots, (0,0), qr_dots)
     main_img.save("./full_qrcode.png")
     os.remove('qrcode.png')

def binary_image_find(response):
    html = response.content
    soup = BeautifulSoup(html, "html.parser")
    b64image = str(soup.find_all('img')[0]).split("base64,")[1].split("\"/")[0]
    return base64.b64decode(b64image)

main()
