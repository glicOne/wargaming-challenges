#!/usr/bin/env python3
import socket
import time
from math import sqrt
# General IRC socket setup
irc = socket.socket()

ircServer = "irc.root-me.org"
ircPort =  6667
ircNick = "glicOne"
channel = "#root-me_challenge"
botName	= "Candy"
ircPass = "re@llySecureP@ss"

def rawSend(data):
    irc.send(str.encode(data))

def ircConnect():
    irc.connect((ircServer, ircPort))
        
def ircMessage(msg, channel):
    rawSend("PRIVMSG " + channel + " :" + msg + "\r\n")

def ircRegister():
    rawSend("USER " + ircNick + "  " + ircNick + " " + ircNick + ":Just testing .\r\n")

def ircSendNick():
    rawSend("NICK " + ircNick + "\r\n")

def ircIndent():
    rawSend("PRIVMSG" + " NICKSERV :identify " + ircPass +"\n")
    
def ircJoin(channel):
    rawSend("JOIN " + channel + "\r\n")
 
def Initialize():
    ircConnect()
    ircRegister()
    ircSendNick()
    ircIndent()
    ircJoin(channel)

    
def ch1(num1, num2):
    return round(sqrt(num1) * num2, 2)

Initialize()

while True:
    data = irc.recv(4096)
    print(data.decode())
    # Some servers have a non-standard implementation of IRC, so we're looking for 'ING' rather than 'PING'
    if "Candy!" in data.decode():
        arrayData = data.decode().split(" / ")
        num1 = arrayData[0].split(":")[-1]
        num2 = arrayData[1]
        print(num1)
        print(num2)
        if num1 == "" or num2 == "":
            continue
        else:
            answer = ch1(int(num1), int(num2))
            print(answer)
            ircMessage("!ep1 -rep " + str(answer), botName)
            continue
    else:
        ircMessage("!ep1", botName)
        time.sleep(.300)
