#!/usr/bin/env python3
import socket
import time
import codecs
# General IRC socket setup
irc = socket.socket()

ircServer = "irc.root-me.org"
ircPort =  6667
ircNick = "glicOne"
channel = "#root-me_challenge"
botName	= "Candy"
ircPass = "re@llySecureP@ss"

def rawSend(data):
    irc.send(str.encode(data))

def ircConnect():
    irc.connect((ircServer, ircPort))
        
def ircMessage(msg, channel):
    rawSend("PRIVMSG " + channel + " :" + msg + "\r\n")

def ircRegister():
    rawSend("USER " + ircNick + "  " + ircNick + " " + ircNick + ":Just testing .\r\n")

def ircSendNick():
    rawSend("NICK " + ircNick + "\r\n")

def ircIndent():
    rawSend("PRIVMSG" + " NICKSERV :identify " + ircPass +"\n")
    
def ircJoin(channel):
    rawSend("JOIN " + channel + "\r\n")
 
def Initialize():
    ircConnect()
    ircRegister()
    ircSendNick()
    ircIndent()
    ircJoin(channel)

    
def ep3(bot_reply):
    return codecs.decode(bot_reply, 'rot_13')

Initialize()

while True:
    data = irc.recv(4096)
    if ":Candy!Candy@root-me.org PRIVMSG" in data.decode():
        arrayData = data.decode().split(":")[-1]
        print(arrayData)
        answer = ep3(str(arrayData))
        print(answer)
        ircMessage("!ep3 -rep " + answer, botName)
        continue
    else:
        ircMessage("!ep3", botName)
        time.sleep(.300)
