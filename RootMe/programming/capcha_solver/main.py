#!/usr/bin/env python3
from urllib import request, parse
from bs4 import BeautifulSoup    
import base64
import numpy as np
import requests
from time import sleep
import subprocess
import os

response = requests.get('http://challenge01.root-me.org/programmation/ch8/')

def main(response):
    while True:
        binary_image = binary_image_find(response)
        write_img(binary_image)
        result = captcha_text()
        print(result)
        os.remove('captcha.png')
        data = parse.urlencode({"cametu":result})
        cookie = {'uid': 'wKgbZFwQ4nMf3SETAymtAg==', 'PHPSESSID' :'3lr5ut30h1n4np76lv9pgau912'}
        headers = {
            'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:63.0) Gecko/20100101 Firefox/63.0',
            'Accept-Language': 'en-US,en;q=0.5',
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
            'Accept-Encoding': 'gzip, deflate',
            'Content-Type': 'application/x-www-form-urlencoded'
        }
        req =  send_post(data, cookie, headers)
        if ('Failed' in req.text):
            main(req)
            continue
        else:
            req.text


def send_post(data, cookie, headers):
    req = requests.post("http://challenge01.root-me.org/programmation/ch8/", data=data, cookies=cookie, headers=headers) # this will make the method "POST"
    return req

def write_img(binary_image):
    x = np.fromstring(binary_image, dtype=np.uint8)
    out_bytes = np.unpackbits(x)
    out_bytes = np.packbits(out_bytes)
    out_bytes.tofile('captcha.png')

def binary_image_find(response):
    html = response.text
    soup = BeautifulSoup(html, "html.parser")
    b64image = str(soup.find_all('img')[0]).split("base64,")[1].split("\"/")[0]
    return base64.b64decode(b64image)

def captcha_text():
    file_handle = open('captcha.png', 'rb')
    result = subprocess.Popen(['gocr -i captcha.png'], shell=True, stdout=subprocess.PIPE).communicate()[0]
    file_handle.close
    result = result.decode().replace('\n', '')
    result = result.replace(' ', '')
    result = result.replace(',', '')
    result = result.replace('\'', '')
    result = result.replace('_', '')
    result = result.replace('`', '')
    result = result.replace('^', '')
    result = result.replace('>', '')
    result = result.replace('<', '')
    result = result.replace('%', '')
    result = result.replace('.', '')
    result = result.replace('$', '')
    result = result.replace('#', '')
    result = result.replace(':', '')
    result = result.replace(';', '')
    result = result.replace('?', '')
    return result

main(response)
