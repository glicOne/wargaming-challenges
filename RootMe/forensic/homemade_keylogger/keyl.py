import struct 

in_file = open( "ch24", "rb" ); # Open the file in the read-binary mode
FORMAT = 'llHHI'
EVENT_SIZE = struct.calcsize(FORMAT)

#open file in binary mode

event = in_file.read(EVENT_SIZE)
keycodes = ''
while event:
    (tv_sec, tv_usec, type, code, value) = struct.unpack(FORMAT, event)

    if type != 0 or code != 0 or value != 0:
	if type != 1:
		keycodes += chr(value)
        print("Event type %u, code %u, value %u at %d.%d" % \
            (type, code, value, tv_sec, tv_usec))
    else:
        # Events with code, type and value == 0 are "separator" events
        print("===========================================")

    event = in_file.read(EVENT_SIZE)
print(keycodes)
in_file.close()
